const fs = require('fs');
const super_agent = require('superagent');

// Building Promises
const readFilePromise = file => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err) reject('I could not find that file 😢');
            resolve(data);
        });
    });
};
// Building Promises
const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, err => {
            if (err) reject('I could not write that file 😢');
            resolve('success');
        });
    });
};

const getDogAndCreateDog = async () => {
    try {
        const result = await readFilePromise(`${__dirname}/perro.txt`);
        const response = await super_agent.get(`https://dog.ceo/api/breed/${result.toString()}/images/random`);
        // Return the last resolve from promise optional
        return await writeFilePromise('file-promise.txt', response.body.message);
    } catch (err) {
        if (typeof err === 'object' &&
            !Array.isArray(err) &&
            err !== null) {
            console.log(err.response.body.message);
        } else {
            console.log(err);
        }
    }
};

// Consuming Promises with AsyncAwait
getDogAndCreateDog().then(r => console.log('created', r));