const fs = require('fs');
const super_agent = require('superagent');

// Building Promises
const readFilePromise = file => {
    return new Promise((resolve, reject) => {
        fs.readFile(file, (err, data) => {
            if (err) reject('I could not find that file 😢');
            resolve(data);
        });
    });
};
// Building Promises
const writeFilePromise = (file, data) => {
    return new Promise((resolve, reject) => {
        fs.writeFile(file, data, err => {
            if (err) reject('I could not write that file 😢');
            resolve('success');
        });
    });
};
// Execute the promise
readFilePromise(`${__dirname}/perro.txt`)
    .then(result => {
        return super_agent.get(`https://dog.ceo/api/breed/${result.toString()}/images/random`);
    })
    .then(response => {
        return writeFilePromise('file-promise.txt', response.body.message);
    })
    .then((res) => console.log('Random dog image saved to file!', res))
    .catch(err => {
        // If is an object
        if (typeof err === 'object' &&
            !Array.isArray(err) &&
            err !== null) {
            // console.log(err.status);
            console.log(err.response.body.message);
        } else {
            console.log(err);
        }
    });

// Consuming Promises with AsyncAwait