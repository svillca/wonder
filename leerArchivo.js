const fs = require('fs');
const super_agent = require('superagent');

// The Problem with Callbacks Callback Hell
fs.readFile(`${__dirname}/perro.txt`, (err, data) => {
    console.log('leyendo', data.toString());
    const URL = `https://dog.ceo/api/breed/${data.toString()}/images/random`;
    super_agent
        .get(URL)
        .end((err, res) => {

            if (err) return console.log(err.message);

            console.log('leyendo', res.body);

            fs.writeFile('dog-image-created.txt', res.body.message, err => {
                if (err) return console.log(err.message);

                console.log('archivo creado');
            });
        });
});

// From Callback Hell to Promises
fs.readFile(`${__dirname}/perro.txt`, (err, data) => {
    const URL = `https://dog.ceo/api/breed/${data.toString()}/images/random`;
    super_agent
        .get(URL)
        .then((res) => {
            fs.writeFile('dog-image-created-ii.txt', res.body.message, err => {
                if (err) return console.log(err.message);
                console.log('archivo creado');
            });
        }).catch(err => console.log(err.message));
});